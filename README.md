# systemd-mount (examples)
This was just a little project of mine. I dont like to write fstab entiries, but looove to use systemd units for everything. So why not use systemd to mount `/home`, `/boot/efi`, `/home/share`? :D

## How to..
1. Just edit these files to match your setup (but be careful, file-names MUST be the same as mount points)
2. move them to `/etc/systemd/system/`
3. enable them `systemctl enable home.automount home-share.automount boot-efi.automount` 
4. comment out these mounts in your `/etc/fstab` (`#`)
5. reboot

## links
[systemd.mount](https://www.freedesktop.org/software/systemd/man/systemd.mount.html)

[systemd.automount](https://www.freedesktop.org/software/systemd/man/systemd.automount.html)